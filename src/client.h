#include <cstdlib>
#include <ostream>
#include <iostream>
#include <thread>

#include <stdio.h>
#include <readline/readline.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>

#include "aes.h"

#define PORT 8194
#define BUFFER_SIZE 2048 

class Client{
	public:

	static void listen4chat(int client_fd, string_view name, Aes* encryptor)
	{
		while(true){
			char buffer[BUFFER_SIZE] = { 0 };
			(void)read(client_fd, buffer, BUFFER_SIZE);
			string msg = encryptor->decrypt(buffer);
			if(msg.find("_TERM") != string::npos){
				break;
			}
			printf("\r%s", msg.c_str());
			cout << "\n" << name << flush;
		}
	}

	int mkSock()
	{
		int client_fd = socket(AF_INET, SOCK_STREAM, 0);
		if(client_fd < 0){
			printf("\n Socket creation error \n");
			exit(9);
		}
		return client_fd;
	}

	void connStat(int client_fd, struct sockaddr* serv_addr, int servAddrSz)
	{
		int stat = connect(client_fd, serv_addr, servAddrSz);
		if (stat >= 0)
			return;

		cerr << "\nConnection Failed \n";
		exit(7);
		
	}

	// Convert IPv4 and IPv6 addresses from text to binary form
	void ipCheck(int inet_pton)
	{
		if (inet_pton <= 0){
			printf("\nInvalid address/ Address not supported \n");
			exit(8);
		}
	}

	void sendMsg(int client_fd, string name, Aes* encryptor)
	{
		while(true){
			string msg = name;
			string tmp;
			
			tmp = readline(name.c_str());
			msg.append(tmp);

			string actual = encryptor->encrypt(msg);
			send(client_fd, actual.c_str(), actual.length(), 0);
			if(".leave" == tmp)
				break;
		}
	}
};
