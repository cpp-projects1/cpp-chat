// Server side C/C++ program to demonstrate Socket
// programming

#include "server.h"
#include <algorithm>

using std::string, std::cout, std::endl, std::string_view, std::thread,
	  std::vector, std::shared_ptr;


int main(int argc, char const* argv[])
{
	int server_fd;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);

	shared_ptr<Aes> decryptor(new Aes());

	unique_ptr<Server> srv(new Server());

	server_fd = srv->mkSock();

	srv->connStat(server_fd, &opt, sizeof(opt));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	srv->bindPort(server_fd, (struct sockaddr*)&address, addrlen);

	srv->setListen(server_fd);

	srv->serve(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen, decryptor);

	// closing the listening socket
	shutdown(server_fd, SHUT_RDWR);
	return 0;
}
