#include <iostream>
#include <iomanip>

#include <crypt.h>
#include <cryptopp/modes.h>
#include <cryptopp/aes.h>
#include <cryptopp/filters.h>

#include "b64.h"

using namespace std;

class Aes {
	private:
		CryptoPP::byte key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
	public:
		Aes()
		{
			memset( key, 0x00, CryptoPP::AES::DEFAULT_KEYLENGTH );
			memset( iv, 0x00, CryptoPP::AES::BLOCKSIZE );
			memcpy(key, "L!f315bvTa9re@m.", 16);
			memcpy(iv, "15P@55WoRds*xbuT", 16);
		};

		string encrypt(string plaintext)
		{
			string ciphertext;
			CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
			CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );

			CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( ciphertext ) );
			stfEncryptor.Put( reinterpret_cast<const unsigned char*>( plaintext.c_str() ), plaintext.length() + 1 );
			stfEncryptor.MessageEnd();
			ciphertext = macaron::Base64::Encode(ciphertext);
			return ciphertext;
		};

		string decrypt(string transmittedText)
		{
			string plaintext;
			string ciphertext;
			macaron::Base64::Decode(transmittedText, ciphertext);
			CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
			CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

			CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( plaintext ) );
			stfDecryptor.Put( reinterpret_cast<const unsigned char*>( ciphertext.c_str() ), ciphertext.size() );
			stfDecryptor.MessageEnd();
			return plaintext;
		};

};
