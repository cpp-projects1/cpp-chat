#include <iostream>
#include <thread>
#include <vector>

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include "aes.h"

#define PORT 8194
#define BUFFER_SIZE 2048 

vector<int> socks;

class Server{
	public:

	static void clientThread(int new_socket, shared_ptr<Aes> decryptor)
	{
		char tmpName[BUFFER_SIZE] = { 0 };
		(void)read(new_socket, tmpName, BUFFER_SIZE);
		string name(tmpName);
		string userName = name;

		while(true) {
			char buffer[BUFFER_SIZE] = { 0 };
			if(!read(new_socket, buffer, BUFFER_SIZE)){
				break;
			}
			string msg(buffer);
			for(ulong i = 0; i < socks.size(); i++){
				if(socks[i] == new_socket){
					continue;
				}
				send(socks[i], msg.c_str(), msg.length(), 0);
			}

			msg = decryptor->decrypt(msg);
			cout << msg << endl;
			if (msg.find(".leave") != string::npos) {
				msg = "_TERM";
				msg = decryptor->encrypt(msg);
				send(new_socket, msg.c_str(), msg.length(), 0);
				break;
			}
		}

		close(new_socket);
	}

	int mkSock()
	{
		int server_fd = socket(AF_INET, SOCK_STREAM, 0);
		if(server_fd < 0){
			printf("\n Socket creation error \n");
			exit(9);
		}
		return server_fd;
	}

	// Forcefully attaching socket to the port
	void connStat(int server_fd, int* opt, int optSize)
	{
		if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, opt, optSize)) {
			perror("setsockopt");
			exit(7);
		}
		
	}

	void bindPort(int server_fd, struct sockaddr* serv_addr, int servAddrSz)
	{
		if (bind(server_fd, serv_addr, servAddrSz) >= 0) 
			return;

		perror("bind failed");
		exit(6);
	}

	void setListen(int server_fd)
	{
		if (listen(server_fd, 3) >= 0)
			return;
		perror("listen");
		exit(EXIT_FAILURE);
	}

	void serve(int server_fd, struct sockaddr* serv_addr, unsigned int* servAddrSz, shared_ptr<Aes> decryptor)
	{
		int i = 0;
		while(true) {
			socks.push_back(accept(server_fd, serv_addr, servAddrSz));
			if (socks[i] < 0) {
				perror("accept");
				exit(5);
			}

			thread t(this->clientThread, socks[i], decryptor);

			t.detach();
			i++;
		} 
	}
};
