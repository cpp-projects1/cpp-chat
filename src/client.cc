// Client side C/C++ program to demonstrate Socket
// programming

#include "client.h"

using std::cout, std::cin, std::flush, std::string, std::string_view,
	  std::thread;

int main(int argc, char const* argv[])
{
	int client_fd;
	struct sockaddr_in serv_addr;

	// not using smart ptr, this function has ownership
	Aes* encryptor = new Aes();
	unique_ptr<Client> client(new Client());

	string host;
	cout << "Please enter host: ";
	cin >> host;

	string name;
	cout << "Please enter username: ";
	cin >> name;

	name.append("> ");

	client_fd = client->mkSock();

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	client->ipCheck(inet_pton(AF_INET, host.c_str(), &serv_addr.sin_addr));

	client->connStat(client_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

	send(client_fd, name.c_str(), name.length(), 0);
	thread t(client->listen4chat, client_fd, name, encryptor);

	client->sendMsg(client_fd, name, encryptor);

	t.join();
	free(encryptor);
	close(client_fd);
	return 0;
}
