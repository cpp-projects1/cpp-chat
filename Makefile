CC = g++
CFLAGS = -Wall -l:libcryptopp.a
SRC = src/

main: server client

server: $(SRC)server.cc $(SRC)aes.h $(SRC)b64.h $(SRC)server.h
	$(CC) $(SRC)server.cc $(CFLAGS) -o server

client: $(SRC)client.cc $(SRC)aes.h $(SRC)b64.h
	$(CC) $(SRC)client.cc -lreadline $(CFLAGS) -o client

test: server client
	$(CC) tests/clientTests.cc -lgtest -lgtest_main $(CFLAGS) -o tests/clientTests
	$(CC) tests/serverTests.cc -lgtest -lgtest_main $(CFLAGS) -o tests/serverTests
	tests/serverTests
	tests/clientTests

clean:
	rm server client
	rm tests/clientTests
	rm tests/serverTests
