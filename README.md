# CPP Chat

## Purpose

The purpose of this project is to expand my knowledge of C++ and Sockets.
Project grew to include exploration of smart pointers, linking, multithreading,
encryption in C++, and some C++ libraries.


## TODO

[X] fix prompt display bug
    [X] on send
    [X] on recieve
[X] fix message spacing
[X] fix arrow keys inputing raw keycodes 
[X] handle force quits from client
[X] ask for host url/ip at client launch
[X] pick a better port than 8080
[-] fix typed (but not sent) messages disappearing when recieving a message (this is a known limitation)
[X] exit client properly (no unfinished thread wanings)
[X] implement encryption (proof-of-concept)
[X] fix crashing due to message not being a multiple of blocksize -- base64 encode fixed it

## Dependencies

### crypto++

reason:  
aes encryption

(Debian) install:  
`sudo apt-get install libcrypto++-dev libcrypto++-doc libcrypto++-utils`

### GNU readline

reason:  
easy way to get input but not let arrow keys, and other pollute input

(Debian) install:
`sudo apt install libreadline-dev`

### base64.h

included in files, but worthy mention for fixing crashing due to encryption
producing NULL characters

## Testing

Testing is present, but a work in progress. This is for two main reasons:

1. I am new to gtest - the google C++ testing library I chose for this project
1. Testing socket connections and threads requires thinking rather different than normal proceedural/OOP tests and I am still new to this
