#include "gtest/gtest.h"
#include <gtest/gtest.h>

#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#include "../src/client.h"

// Due to the nature of the program, no test pass/fail necessary
// if the method calls run, they are passing, otherwise they will crash

TEST(Client, mkSock)
{
	unique_ptr<Client> client(new Client());
	(void)client->mkSock();
}

TEST(Client, ipCheck)
{
	struct sockaddr_in serv_addr;

	unique_ptr<Client> client(new Client());
	(void)client->mkSock();

	string host = "127.0.0.1";

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	client->ipCheck(inet_pton(AF_INET, host.c_str(), &serv_addr.sin_addr));
}

TEST(Client, connStat_fails)
{
	int client_fd;
	struct sockaddr_in serv_addr;

	unique_ptr<Client> client(new Client());
	client_fd = client->mkSock();

	string host = "127.0.0.1";

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	client->ipCheck(inet_pton(AF_INET, host.c_str(), &serv_addr.sin_addr));

	EXPECT_EXIT(client->connStat(client_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)), testing::ExitedWithCode(7), "Connection Failed");


}

TEST(Client, connStat_succeeds)
{
	int client_fd;
	struct sockaddr_in serv_addr;

	unique_ptr<Client> client(new Client());
	client_fd = client->mkSock();

	string host = "127.0.0.1";

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	client->ipCheck(inet_pton(AF_INET, host.c_str(), &serv_addr.sin_addr));

	pid_t pid;
	pid = fork();

	if(0 == pid){
		char cmd[] = "./server";
		execl(cmd, "../server", NULL);
	} else {
		sleep(1);
		client->connStat(client_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
		kill(pid, SIGTERM);
		waitpid(pid, NULL, WNOHANG);
	}

}
