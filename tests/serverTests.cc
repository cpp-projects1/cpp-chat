#include <gtest/gtest.h>

#include "../src/server.h"

// Due to the nature of the program, no test pass/fail necessary
// if the method calls run, they are passing, otherwise they will crash

TEST(Server, mkSock)
{
	unique_ptr<Server> srv(new Server());
	(void)srv->mkSock();
}

TEST(Server, connStat)
{
	unique_ptr<Server> srv(new Server());
	int server_fd;
	int opt = 1;
	server_fd = srv->mkSock();

	srv->connStat(server_fd, &opt, sizeof(opt));
}

TEST(Server, bindPort)
{
	unique_ptr<Server> srv(new Server());
	int server_fd;
	int opt = 1;

	struct sockaddr_in address;
	int addrlen = sizeof(address);
	server_fd = srv->mkSock();
	srv->connStat(server_fd, &opt, sizeof(opt));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	srv->bindPort(server_fd, (struct sockaddr*)&address, addrlen);
}

TEST(Server, setListen)
{
	unique_ptr<Server> srv(new Server());
	int server_fd;
	int opt = 1;

	struct sockaddr_in address;
	int addrlen = sizeof(address);
	server_fd = srv->mkSock();
	srv->connStat(server_fd, &opt, sizeof(opt));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	srv->bindPort(server_fd, (struct sockaddr*)&address, addrlen);

	srv->setListen(server_fd);
}

TEST(Server, serve)
{
	unique_ptr<Server> srv(new Server());
	shared_ptr<Aes> decryptor(new Aes());
	int server_fd;
	int opt = 1;

	struct sockaddr_in address;
	int addrlen = sizeof(address);
	server_fd = srv->mkSock();
	srv->connStat(server_fd, &opt, sizeof(opt));

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(PORT);

	srv->bindPort(server_fd, (struct sockaddr*)&address, addrlen);

	srv->setListen(server_fd);
	
	// still thinking about how to implement testing this - hangs test suite otherwise
	//srv->serve(server_fd, (struct sockaddr*)&address, (socklen_t*)&addrlen, decryptor);

	shutdown(server_fd, SHUT_RDWR);
}
